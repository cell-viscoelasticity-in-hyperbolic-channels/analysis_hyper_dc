import numpy as np
from pandas.core.series import Series

def stress_wc60_lc500(flow_rate):
    """
    Calculate the stress acting in the stable hyperbolic region
    in the wc=60 µm, Lc=500 µm channel.

    Parameters
    ----------
    flow_rate: channel flow rate in ul/s

    Returns
    -------
    Stress in Pa
    """
    return 511.915230 * flow_rate**0.770230


def stress_wc60_lc500_20230322(flow_rate):
    """
    Calculate the stress acting in the stable hyperbolic region in the wc=60
    µm, Lc=500 µm channel. Calculation based on results for small strains
    (< 0.1) in notebook `20230223_stress_analysis_beads_3.0.ipynb`.

    Parameters
    ----------
    flow_rate: channel flow rate in ul/s

    Returns
    -------
    Stress in Pa
    """
    return 3420 * flow_rate**1.49


def stress_wc60_lc500_20230403(flow_rate):
    """
    Calculate the stress acting in the stable hyperbolic region in the wc=60
    µm, Lc=500 µm channel. Calculation based on results for small strains
    (< 0.1) in notebook `20230403_stress_analysis_beads_3.1.ipynb`.

    Parameters
    ----------
    flow_rate: channel flow rate in ul/s

    Returns
    -------
    Stress in Pa
    """
    if flow_rate < 0:
        return 0
    if flow_rate <= 0.07:
        return 4530.6868 * flow_rate**1.6374
    else:
        return 765.6 * flow_rate + 4.6321


def stress_wc100_lc500(flow_rate):
    """
    Calculate the stress acting in the stable hyperbolic region
    in the wc=100 µm, Lc=500 µm channel.

    Parameters
    ----------
    flow_rate: channel flow rate in ul/s

    Returns
    -------
    Stress in Pa
    """
    return 295.533250 * flow_rate**0.720492


def stress_wc60_lc500_extension_rate(extension_rate):
    """
    Calculate the stress acting in the wc=60 µm, Lc=500 µm channel dependent on
    the extension rate.

    Parameters
    ----------
    extension_rate: extension rate in 1/s.

    Returns
    -------
    Stress in Pa
    """
    if extension_rate < 0:
        return 0
    if extension_rate <= 100:
        return 0.044 * extension_rate**1.562
    else:
        return 0.58482 * extension_rate - 0.05773


def stress_wc60_lc500_extension_rate_20230403(extension_rate):
    """
    Calculate the stress acting in the wc=60 µm, Lc=500 µm channel dependent on
    the extension rate. Calculation based on results for small strains (< 0.1)
    in notebook `20230403_stress_analysis_beads_3.1.ipynb`.

    Parameters
    ----------
    extension_rate: *float*
        Extension rate in 1/s. Array input will not work.

    Returns
    -------
    Stress in Pa
    """
    if extension_rate < 0:
        return 0
    if extension_rate <= 100:
        return 0.0315 * extension_rate**1.6302
    else:
        return 0.5821 * extension_rate - 0.7703


def get_normal_stress(extension_rate, k_low, n_low, m_high, c_high):
    """
    Helper function that returns stress for extension rate in 1/s dependend on
    the model. Stress follow power law for extension_rate <= 100 1/s and linear
    increase for extension_rate > 100 1/s. Stress = 0 when extension_rate < 0.
    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *Series*
        Extension rate in 1/s.
    k_low: *float*
        Power law amplitude for low extension rates.
    n_low: *float*
        Power law exponent for low extension rates.
    m_high: *float*
        Slope of linear increase for high extension rates.
    c_high: *float*
        Intercept of linear increase for high extension rates.

    Returns
    -------
    Stress in Pa.
    """
    if isinstance(extension_rate, (int, float)):
        if extension_rate < 0:
            return 0
        if extension_rate <= 100:
            return k_low * extension_rate**n_low
        else:
            return m_high * extension_rate + c_high
    elif isinstance(extension_rate, (np.ndarray, np.generic, Series)):
        stress = np.zeros_like(extension_rate)
        idx_0_100 = (0 < extension_rate) & (extension_rate <= 100)
        stress[idx_0_100] = k_low * extension_rate[idx_0_100]**n_low

        idx_gr_100 = 100 < extension_rate
        stress[idx_gr_100] = m_high * extension_rate[idx_gr_100] + c_high

        return stress
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(extension_rate)))


def get_normal_stress_flow_rate(flow_rate, k_low, n_low, m_high, c_high):
    """
    Helper function that returns stress for flow rate in uL/s dependent on
    the model. Stress follow power law for flow_rate <= 0.07 uL/s and linear
    increase for flow_rate > 0.07 uL/s. Stress = 0 when flow_rate < 0.
    Parameters
    ----------
    flow_rate: *float* or *ndarray* or *Series*
        Flow rate in uL/s.
    k_low: *float*
        Power law amplitude for low extension rates.
    n_low: *float*
        Power law exponent for low extension rates.
    m_high: *float*
        Slope of linear increase for high extension rates.
    c_high: *float*
        Intercept of linear increase for high extension rates.

    Returns
    -------
    Stress in Pa.
    """
    if isinstance(flow_rate, (int, float)):
        if flow_rate < 0:
            return 0
        if flow_rate <= 0.07:
            return k_low * flow_rate**n_low
        else:
            return m_high * flow_rate + c_high
    elif isinstance(flow_rate, (np.ndarray, np.generic, Series)):
        stress = np.zeros_like(flow_rate)
        idx_0_100 = (0 < flow_rate) & (flow_rate <= 0.07)
        stress[idx_0_100] = k_low * flow_rate[idx_0_100]**n_low

        idx_gr_100 = 0.07 < flow_rate
        stress[idx_gr_100] = m_high * flow_rate[idx_gr_100] + c_high

        return stress
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(flow_rate)))


def stress_wc60_lc500_extension_rate_20230622(extension_rate):
    """
    Calculate the stress acting in the wc=60 µm, Lc=500 µm channel dependent on
    the extension rate. Calculation is done for strain based on inertia ratio.
    Calculation based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *Series*
        Extension rate in 1/s. Array input will not work.

    Returns
    -------
    Stress in Pa
    """
    k_low = 0.1232
    n_low = 1.3684
    m_high = 0.6307
    c_high = 4.1553
    return get_normal_stress(extension_rate, k_low, n_low, m_high, c_high)


def stress_wc60_lc500_flow_rate_20230622(flow_rate):
    """
    Calculate the stress acting in the wc=60 µm, Lc=500 µm channel dependent on
    the flow rate. Calculation is done for strain based on inertia ratio.
    Calculation based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    flow_rate: *float* or *ndarray* or *Series*
        Flow rate in uL/s.

    Returns
    -------
    Stress in Pa
    """
    k_low = 2262.17
    n_low = 1.3283
    m_high = 855.32
    c_high = 6.2634
    return get_normal_stress(flow_rate, k_low, n_low, m_high, c_high)


def shear_stress_wc60_20230616(size, flow_rate):
    """
    Calculate the shear stress acting in the straight channel with wc=60 µm
    dependent on the object size (diameter in um) and flow rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230607_paa_beads_outside_hyper.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return (1695 * flow_rate + 22) * confinement**2


def shear_stress_hyper_wc60_lc500_20230616(x, size, flow_rate):
    """
    Calculate the shear stress acting in the hyperbolic region with wc=60 µm
    and Lc=500 µm dependent on the x-position, object size (diameter in um) and
    flow rate. Calculation based on results for small strains (< 0.1) in
    notebook `20230607_paa_beads_outside_hyper.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    stress = shear_stress_wc60_20230616(size, flow_rate) * x / hyper_length

    if type(stress) in [int, float]:
        if stress < 0:
            stress = 0
    else:
        if len(stress) > 0:
            stress[stress < 0] = 0

    return stress


def normal_stress_hyper_wc60_lc500_20230616(extension_rate):
    """
    Calculate the first normal stress difference acting in the hyperbolic
    region with wc=60 µm and Lc=500 µm dependent on the extension rate.
    Calculation based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *pandas.Series*
        Extension rate in 1/s.

    Returns
    -------
    Shear stress in Pa
    """
    k_low = 0.0413
    n_low = 1.5348
    m_high = 0.4285
    c_high = 5.6141
    return get_normal_stress(extension_rate, k_low, n_low, m_high, c_high)


def total_stress_hyper_wc60_lc500_20230616(extension_rate, x, size, flow_rate):
    """
    Calculate the total stress acting in the hyperbolic region
    with wc=60 µm and Lc=500 µm dependent on the extension rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_20230616(x, size, flow_rate)
    normal = normal_stress_hyper_wc60_lc500_20230616(extension_rate)
    return shear + normal


def shear_stress_wc60_20230621(size, flow_rate):
    """
    Calculate the shear stress acting in the straight channel with wc=60 µm
    dependent on the object size (diameter in um) and flow rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230607_paa_beads_outside_hyper.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return 1046.30531 * flow_rate**0.74944558 * confinement**2


def shear_stress_hyper_wc60_lc500_20230621(x, size, flow_rate):
    """
    Calculate the shear stress acting in the hyperbolic region with wc=60 µm
    and Lc=500 µm dependent on the x-position, object size (diameter in um) and
    flow rate. Calculation based on results for small strains (< 0.1) in
    notebook `20230607_paa_beads_outside_hyper.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    stress = shear_stress_wc60_20230621(size, flow_rate) * x / hyper_length

    if isinstance(stress, (int, float)):
        if stress < 0:
            stress = 0
    elif isinstance(stress, (np.ndarray, np.generic, Series)):
        if len(stress) > 0:
            stress[stress < 0] = 0
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(stress)))

    return stress


def normal_stress_hyper_wc60_lc500_20230621(extension_rate):
    """
    Calculate the first normal stress difference acting in the hyperbolic
    region with wc=60 µm and Lc=500 µm dependent on the extension rate.
    Calculation based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *pandas.Series*
        Extension rate in 1/s.

    Returns
    -------
    Shear stress in Pa
    """
    k_low = 0.0432
    n_low = 1.5188
    m_high = 0.4745
    c_high = -0.2917
    return get_normal_stress(extension_rate, k_low, n_low, m_high, c_high)


def total_stress_hyper_wc60_lc500_20230621(extension_rate, x, size, flow_rate):
    """
    Calculate the total stress acting in the hyperbolic region
    with wc=60 µm and Lc=500 µm dependent on the extension rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230615_stress_analysis_beads_in_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_20230621(x, size, flow_rate)
    normal = normal_stress_hyper_wc60_lc500_20230621(extension_rate)
    return shear + normal


def shear_stress_inlet_wu250_20230801(size, flow_rate):
    """
    Calculate the shear stress acting in the straight channel with wc=60 µm
    dependent on the object size (diameter in um) and flow rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230607_paa_beads_outside_hyper.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return (139.34 * flow_rate + 8.75) * confinement**2


def shear_stress_hyper_wc60_lc500_20230801(x, size, flow_rate):
    """
    Calculate the shear stress acting in the hyperbolic region with wc=60 µm
    and Lc=500 µm dependent on the x-position, object size (diameter in um) and
    flow rate. Calculation based on results for small strains (< 0.1) in
    notebook `20230607_paa_beads_outside_hyper.ipynb` and
    `20230731_paa_beads_before_hyper.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    stress_inlet = shear_stress_inlet_wu250_20230801(size, flow_rate)
    stress_channel = shear_stress_wc60_20230621(size, flow_rate)
    stress = (stress_channel - stress_inlet) * x / hyper_length + stress_inlet

    if isinstance(stress, (int, float)):
        if stress < 0:
            stress = 0
    elif isinstance(stress, (np.ndarray, np.generic, Series)):
        if len(stress) > 0:
            stress[stress < 0] = 0
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(stress)))
    return stress


def total_stress_hyper_wc60_lc500_20230801(extension_rate, x, size, flow_rate):
    """
    Calculate the total stress acting in the hyperbolic region
    with wc=60 µm and Lc=500 µm dependent on the extension rate. Calculation
    based on results for small strains (< 0.1) in notebook
    `20230621_stress_analysis_beads_in_hyper.ipynb` and
    `20230731_paa_beads_before_hyper.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_20230801(x, size, flow_rate)
    normal = normal_stress_hyper_wc60_lc500_20230621(extension_rate)
    return shear + normal


def shear_stress_inlet_wu250_ell_20230816(size, flow_rate):
    """
    Calculate the shear stress influence on the ellipse strain acting in the
    straight channel with upper channel width wu=250 µm dependent on the object
    size (diameter in um) and flow rate. Calculation based on results for small
    strains (< 0.1) in notebook
    `20230814_paa_beads_outside_hyper_ellipse_analysis.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return 105.33 * flow_rate * confinement**2


def shear_stress_channel_wc60_ell_20230816(size, flow_rate):
    """
    Calculate the shear stress influence on the ellipse strain acting in the
    straight channel with wc=60 µm dependent on the object size (diameter in
    um) and flow rate. Calculation based on results for small strains (< 0.1)
    in notebook `20230814_paa_beads_outside_hyper_ellipse_analysis.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return 1882.96 * flow_rate * confinement**2


def shear_stress_hyper_wc60_lc500_ell_20230816(x, size, flow_rate):
    """
    Calculate the shear stress influence on the ellipse strain acting in the
    hyperbolic region with wc=60 µm and Lc=500 µm dependent on the x-position,
    object size (diameter in um) and flow rate. Calculation based on results
    for small strains (< 0.1) in notebook
    `20230814_paa_beads_outside_hyper_ellipse_analysis.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    stress_inlet = shear_stress_inlet_wu250_ell_20230816(size, flow_rate)
    stress_channel = shear_stress_channel_wc60_ell_20230816(size, flow_rate)
    stress = (stress_channel - stress_inlet) * x / hyper_length + stress_inlet

    if isinstance(stress, (int, float)):
        if x < 0:
            stress = stress_inlet
        if stress < 0:
            stress = 0
    elif isinstance(stress, (np.ndarray, np.generic, Series)):
        if len(stress) > 0:
            stress[x < 0] = stress_inlet
            stress[stress < 0] = 0
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(stress)))
    return stress


def normal_stress_hyper_wc60_lc500_ell_20230817(extension_rate):
    """
    Calculate the first normal stress difference based on the ellipse strain
    acting in the hyperbolic region with wc=60 µm and Lc=500 µm dependent on
    the extension rate. Calculation based on results for small strains (< 0.12)
    in notebook `20230816_stress_analysis_hyper_ellipse_strain.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *pandas.Series*
        Extension rate in 1/s.

    Returns
    -------
    Shear stress in Pa
    """
    k_low = 0.1343
    n_low = 1.2559
    m_high = 0.4754
    c_high = -3.8970
    return get_normal_stress(extension_rate, k_low, n_low, m_high, c_high)


def total_stress_hyper_wc60_lc500_ell_20230817(extension_rate, x, size,
                                               flow_rate):
    """
    Calculate the total stress based on the ellipse strain acting in the
    hyperbolic region with wc=60 µm and Lc=500 µm dependent on the extension
    rate. Calculation based on results for small strains (< 0.1) in notebook
    `20230814_paa_beads_outside_hyper_ellipse_analysis.ipynb` and
    `20230816_stress_analysis_hyper_ellipse_strain.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_ell_20230816(x, size, flow_rate)
    normal = normal_stress_hyper_wc60_lc500_ell_20230817(extension_rate)
    return shear + normal


def shear_stress_inlet_wu250_net_strain_ir_20231109(size, flow_rate):
    """
    Calculate the shear stress influence on the net tensile strain based on
    'inert_ratio_raw' acting in the straight channel with upper channel width
    wu=250 µm dependent on the object size (diameter in um) and flow rate.
    Calculation based on results for small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return 138.33 * flow_rate * confinement**2


def shear_stress_channel_wc60_net_strain_ir_20231109(size, flow_rate):
    """
    Calculate the shear stress influence on the net tensile strain based on
    'inert_ratio_raw' acting in the straight channel with upper channel width
    wc=60 µm dependent on the object size (diameter in um) and flow rate.
    Calculation based on results for small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb`.

    Parameters
    ----------
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    channel_height = 30
    confinement = size / channel_height
    return 1282.7 * flow_rate**0.882 * confinement**2


def shear_stress_hyper_wc60_lc500_net_strain_ir_20231109(x, size, flow_rate):
    """
    Calculate the shear stress influence on the net tensile strain based on
    'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm and
    Lc=500 µm dependent on the x-position, object size (diameter in um) and
    flow rate. Calculation based on results for small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    stress_inlet = shear_stress_inlet_wu250_net_strain_ir_20231109(size,
                                                                   flow_rate)
    stress_channel = shear_stress_channel_wc60_net_strain_ir_20231109(size,
                                                                      flow_rate)
    stress = (stress_channel - stress_inlet) * x / hyper_length + stress_inlet

    if isinstance(stress, (int, float)):
        if x < 0:
            stress = stress_inlet
        if stress < 0:
            stress = 0
    elif isinstance(stress, (np.ndarray, np.generic, Series)):
        if len(stress) > 0:
            stress[x < 0] = stress_inlet
            stress[stress < 0] = 0
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(stress)))
    return stress


def normal_stress_hyper_wc60_lc500_net_strain_ir_20231109(extension_rate):
    """
    Calculate the first normal stress difference based on the net tensile
    strain from 'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm
    and Lc=500 µm dependent on the extension rate. Calculation based on results
    for small strains (< 0.12) in notebook
    `20231109_stress_analysis_hyper_net_strain_ir.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *pandas.Series*
        Extension rate in 1/s.

    Returns
    -------
    Shear stress in Pa
    """
    return 0.4956 * extension_rate


def total_stress_hyper_wc60_lc500_net_strain_ir_20231109(extension_rate, x,
                                                         size, flow_rate):
    """
    Calculate the total stress based on the net tensile strain from
    'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm and
    Lc=500 µm dependent on the extension rate. Calculation based on results for
    small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb` and
    `20231109_stress_analysis_hyper_net_strain_ir.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_net_strain_ir_20231109(x, size,
                                                                 flow_rate)
    normal \
        = normal_stress_hyper_wc60_lc500_net_strain_ir_20231109(extension_rate)
    return shear + normal


def stress_wc60_lc500_flow_rate_net_strain_ir_20231115(flow_rate):
    """
    Calculate the stress acting in the wc=60 µm, Lc=500 µm channel dependent on
    the flow rate. Calculation is done for net tensile strain based on inertia
    ratio. Calculation based on results for small strains (< 0.125) in notebook
    `20231109_stress_analysis_hyper_net_strain_ir.ipynb`.

    Parameters
    ----------
    flow_rate: *float* or *ndarray* or *Series*
        Flow rate in uL/s.

    Returns
    -------
    Stress in Pa
    """
    return 969.45 * flow_rate


def shear_stress_hyper_wc60_lc500_net_strain_ir_20240226(x, size, flow_rate):
    """
    Calculate the shear stress influence on the net tensile strain based on
    'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm and
    Lc=500 µm dependent on the x-position, object size (diameter in um) and
    flow rate. Use a power law to describe the stress function over x with
    power law exponent n=0.88 based on the power law for the influence of the
    shear stress presented in equation 24. Calculation based on results for
    small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb`.

    Parameters
    ----------
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Shear stress in Pa
    """
    hyper_length = 500
    n = 0.88
    stress_inlet = shear_stress_inlet_wu250_net_strain_ir_20231109(size,
                                                                   flow_rate)
    stress_channel = shear_stress_channel_wc60_net_strain_ir_20231109(size,
                                                                      flow_rate)
    stress = ((stress_channel - stress_inlet) * (x / hyper_length)**n
              + stress_inlet)

    if isinstance(stress, (int, float)):
        if x < 0:
            stress = stress_inlet
        if stress < 0:
            stress = 0
    elif isinstance(stress, (np.ndarray, np.generic, Series)):
        if len(stress) > 0:
            stress[x < 0] = stress_inlet
            stress[stress < 0] = 0
    else:
        raise(TypeError,
              "Input type not supported: {}".format(type(stress)))
    return stress


def normal_stress_hyper_wc60_lc500_net_strain_ir_20240226(extension_rate):
    """
    Calculate the first normal stress difference based on the net tensile
    strain from 'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm
    and Lc=500 µm dependent on the extension rate. Calculation based on results
    for small strains (< 0.125) in notebook
    `20231109_stress_analysis_hyper_net_strain_ir.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *ndarray* or *pandas.Series*
        Extension rate in 1/s.

    Returns
    -------
    Shear stress in Pa
    """
    return 0.155 * extension_rate**1.225


def total_stress_hyper_wc60_lc500_net_strain_ir_20240226(extension_rate, x,
                                                         size, flow_rate):
    """
    Calculate the total stress based on the net tensile strain from
    'inert_ratio_raw' acting in the hyperbolic region with wc=60 µm and
    Lc=500 µm dependent on the extension rate. Calculation based on results for
    small strains (< 0.1) in notebook
    `20231108_paa_beads_outside_hyper_net_strain_inert.ipynb` and
    `20231109_stress_analysis_hyper_net_strain_ir.ipynb`.

    Parameters
    ----------
    extension_rate: *float* or *array*
        Extension rate in 1/s.
    x: *float* or *array*
        x-position in um in hyperbolic region in flow direction.
    size: *float* or *array*
        Object diameter in um.
    flow_rate: *float* or *array*
        Total flow rate in uL/s

    Returns
    -------
    Stress in Pa
    """
    shear = shear_stress_hyper_wc60_lc500_net_strain_ir_20240226(x, size,
                                                                 flow_rate)
    normal \
        = normal_stress_hyper_wc60_lc500_net_strain_ir_20240226(extension_rate)
    return shear + normal
