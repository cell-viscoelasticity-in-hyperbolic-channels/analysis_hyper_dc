import numpy as np
import pandas as pd


def kelvin_voigt_strain_step_stress(t, emodulus, tau, stress, epsilon_0):
    """
    Return the strain based on a Kelvin-Voigt model with initial strain
    epsilon_0 at t=0.

    Parameters
    ----------
    t: time in seconds
    emodulus: Young's Modulus in Pa
    tau: Time scale of deformation in 1/s
    stress: Constant stress in Pa
    epsilon_0: initial strain (without unit)

    Returns
    -------
    Strain for the given parameters
    """
    return epsilon_0 * np.exp(-tau * t) + stress/emodulus * (1 - np.exp(-tau * t))


def fit_params_kelvin_voigt(fit_result):
    """Write the Kelvin-Voigt parameters in `fit_results` to a dataframe.
    """
    df_fit_paras = pd.DataFrame()
    for feat in ['emodulus', 'tau', 'epsilon_0']:
        df_fit_paras[feat] = [fit_result.best_values[feat]]
        # std error of fit
        df_fit_paras[feat + '_err'] = fit_result.params[feat].stderr
        # relative error of fit
        df_fit_paras[feat + '_err_rel'] = (df_fit_paras[feat + '_err']
                                           / df_fit_paras[feat])
    df_fit_paras['stress'] = [fit_result.best_values['stress']]

    return df_fit_paras


def power_law_creep(t, stress, emodulus, tau_0, beta, t_0):
    """
    Return the strain based on a power-law model for a creep
    experiment at constant stress.

    Parameters
    ----------
    t: time in seconds
    stress: Constant stress in Pa
    emodulus: Young's Modulus in Pa
    tau_0: timescale at emodulus
    beta: fluidity index
    t_0: time retardation - assume creep started at `t_0`

    Returns
    -------
    Strain for the given parameters
    """
    return stress / emodulus * ((t + t_0)/ tau_0)**beta


def fit_params_power_law(fit_result):
    df_fit_paras = pd.DataFrame()
    for feat in ['emodulus', 'tau_0', 'beta', 't_0']:
        df_fit_paras[feat] = [fit_result.best_values[feat]]
        # std error of fit
        df_fit_paras[feat + '_err'] = fit_result.params[feat].stderr
        # relative error of fit
        df_fit_paras[feat + '_err_rel'] = (df_fit_paras[feat + '_err']
                                           / df_fit_paras[feat])
    df_fit_paras['stress'] = [fit_result.best_values['stress']]

    return df_fit_paras
