import numpy as np
from numpy.polynomial import Polynomial
from lmfit import Model, Parameters

# coefficients in quadratic function to correct strain over pos_x_center for
# different regions
STRAIN_ELL_CORR_DICT = {'a': {'s8_15um': 1.1016870464887436e-07,
                              's03_set2': 5.715221923109669e-08,
                              's1_set4': 1.0486872154843036e-07,
                              's4_12um': 3.932121874156038e-08,
                              's4_17um': 5.693690099284393e-08,
                              's4_18um': 1.099017944583307e-07},
                        'b': {'s8_15um': 1.3200378913834888e-05,
                              's03_set2': 6.088497869320402e-06,
                              's1_set4': 6.938321918198811e-06,
                              's4_12um': 1.4489344230861265e-05,
                              's4_17um': 1.2165820369555323e-05,
                              's4_18um': 1.2532483857102073e-05},
                        'c': {'s8_15um': 0.015050416157634057,
                              's03_set2': 0.018930861687800254,
                              's1_set4': 0.014762793195756331,
                              's4_12um': 0.027140419839017407,
                              's4_17um': 0.019758740352945163,
                              's4_18um': 0.013604572712812532}}


def compute_extensional_paras(tr_ds,
                              hyper_start=None, hyper_end=None,
                              stable_region_start=None,
                              filter=False, **filter_kws):
    """
    :param tr_ds: tracked_dataset instance of dctrack
    :return: df: dataframe with additional analysis parameters
    """
    if filter:
        tr_ds.filter_obj_distance = True
        tr_ds.filter_obj_n_events = True
        tr_ds.filter_backward_movement = True
        tr_ds.filter(inplace=True, return_filtered_ds=False, **filter_kws)

    df = tr_ds.get_extensional_paras_dataframe(hyper_start=hyper_start,
                                               hyper_end=hyper_end,
                                               stable_extension_start=stable_region_start
                                               )
    return df


def fit_poly(x, y, degree):
    # need to exclude nans for the Polynomial.fit function
    nan_idx = np.isnan(x) | np.isnan(y)
    x = x[~nan_idx]
    y = np.abs(y[~nan_idx])
    p = Polynomial.fit(x, y, degree).convert()
    return p


def fit_velocity(df, x_feature='x_hyper', degree=5):
    """Return polynomial function poly_net_strain_ell_corr that returns velocity in m/s for poly_net_strain_ell_corr(x) with
    x in um.
    """
    x = df[x_feature].to_numpy()
    y = df['velocity'].to_numpy()
    return fit_poly(x, y, degree)


def fit_feature(df, y_feature, x_feature='x_hyper', degree=6):
    """Return polynomial function poly_net_strain_ell_corr that returns fitted feature for poly_net_strain_ell_corr(x).
    """
    x = df[x_feature].to_numpy()
    y = df[y_feature].to_numpy()
    return fit_poly(x, y, degree)


def extension_rate_from_velocity_poly(x, velocity_polynomial):
    """Return extension rate in 1/s; x in um.
    """
    p = velocity_polynomial
    dp = p.deriv(1)
    return dp(x) * 1e6


def recalculate_time(df, velocity_polynomial,
                     x_feature='x_hyper', time_label='time_new'):
    times = []
    for xx in df[x_feature]:
        x = np.linspace(0, xx, 100)
        t = np.trapz(1 / velocity_polynomial(x), x=x)
        times.append(t)
    df[time_label] = np.array(times) * 1e-6

    return df


def richards_curve(x, a, k, b, nu, q, c):
    """Return Richard's curve for x with paras:
    x: variable
    a: lower (left) asymptote
    k: upper (right) asymptote when c=1
    b: growth rate
    nu: Shifts x-value of maximum growth
    q: related to value at x=0
    c: has influence on upper asymptote
    """
    return a + (k - a) / (c + q * np.exp(-b * x)**(1 / nu))


def dx_richards_curve(x, a, k, b, nu, q, c):
    """Return derivative for x Richard's curve with paras:
    x: variable
    a: lower (left) asymptote
    k: upper (right) asymptote when c=1
    b: growth rate
    nu: Shifts x-value of maximum growth
    q: related to value at x=0
    c: has influence on upper asymptote
    """
    numerator = b * q * (k - a) * np.exp(b * x / nu)
    denominator = nu * (c * np.exp(b * x / nu) + q)**2
    return numerator / denominator


def recalculate_time_rcurve(df, richards_paras,
                            x_feature='x_hyper', time_label='time_new'):
    """
    Recalculate the time for each event based on the velocity curve derived
    from the Richard's curve fit.

    Parameters
    ----------
    df
    richards_paras
    x_feature
    time_label

    Returns
    -------

    """
    times = []
    for xx in df[x_feature]:
        x = np.linspace(0, xx, 100)
        t = np.trapz(1 / richards_curve(x, *richards_paras), x=x)
        times.append(t)
    df[time_label] = np.array(times) * 1e-6

    return df


def fit_velocity_rcurve(df, x_feature='x_hyper'):
    """
    Fit the velocity data with a Richard's curve.

    Parameters
    ----------
    df
    x_feature
    degree

    Returns
    -------

    """
    x = df[x_feature].to_numpy()
    y = np.abs(df['velocity'].to_numpy())

    params = Parameters()
    params.add('a', value=0.005, min=0.001, max=0.01)
    params.add('k', value=0.05, min=0.01, max=1)
    params.add('b', value=0.01, min=0, max=0.1)
    params.add('nu', value=0.1, min=0, max=1)
    params.add('q', value=1, min=0, max=100)
    params.add('c', value=1, min=0, max=10)
    # create fit model
    fmodel = Model(richards_curve)

    fit_result = fmodel.fit(y, params, x=x, nan_policy='omit')

    a = fit_result.best_values['a']
    k = fit_result.best_values['k']
    b = fit_result.best_values['b']
    nu = fit_result.best_values['nu']
    q = fit_result.best_values['q']
    c = fit_result.best_values['c']
    return a, k, b, nu, q, c


def extension_rate_from_rcurve(x, richards_fit_paras):
    return dx_richards_curve(x, *richards_fit_paras) * 1e6


def fit_richards(x, y, tolerance=0.1, kind=None):
    """
    Fit the strain data with a Richard's curve.

    Parameters
    ----------
    x
    y
    tolerance:
        Set the ratio of the tolerance window for the upper and lower
        bounds around the initital value
    kind:
        Type of data that is analyzed. Possible values are: None, 'stress',
        'strain'.

    Returns
    -------
        (a, k, b, nu, q, c)
    """
    a_init = np.mean(y[0])
    a_lower = a_init - tolerance * a_init
    if a_lower < 0:
        a_lower = 0
    a_upper = a_init + tolerance * a_init

    k_init = np.mean(y[-1])
    k_lower = k_init - tolerance * a_init
    k_upper = k_init + tolerance * a_init

    params = Parameters()
    if kind == 'stress':
        params.add('a', value=0)
        params['a'].vary = False
    else:
        params.add('a', value=a_init, min=a_lower, max=a_upper)
    params.add('k', value=k_init, min=k_lower, max=k_upper)
    params.add('b', value=3*k_init, min=k_init, max=10*k_init)
    params.add('nu', value=0.1, min=0, max=1)
    params.add('q', value=1, min=0, max=100)
    params.add('c', value=1, min=0, max=10)
    # params['c'].vary = False
    # create fit model
    fmodel = Model(richards_curve)

    fit_result = fmodel.fit(y, params, x=x, nan_policy='omit')

    a = fit_result.best_values['a']
    k = fit_result.best_values['k']
    b = fit_result.best_values['b']
    nu = fit_result.best_values['nu']
    q = fit_result.best_values['q']
    c = fit_result.best_values['c']
    return a, k, b, nu, q, c


def strain_inert_correction(x):
    """
    Correction for the strain based on inertia ratio caused by image
    distortions over the ROI. x needs to be the centered x-position in
    reference to the ROI length (at ROI center x=0).

    Parameters
    ----------
    x: x-position centered in ROI.

    Returns
    -------
    Delta strain to correct the strain value:
    strain_true = strain_measured = strain_delta
    """
    return 1.9846e-7 * x**2 + 2.4745e-5 * x


def strain_ell_correction(x):
    """
    Correction for the net tensile strain based on fitted ellipse caused by
    image distortions over the ROI. x needs to be the centered x-position in
    reference to the ROI length (at ROI center x=0).

    Parameters
    ----------
    x: x-position centered in ROI.

    Returns
    -------
    Delta strain to correct the strain value:
    strain_true = strain_measured = strain_delta
    """
    return 9.5530e-8 * x**2 + 1.3179e-5 * x + 0.0180
