import numpy as np
from numpy.polynomial import Polynomial
from lmfit import Model, Parameters


def fit_poly(x, y, degree):
    # need to exclude nans for the Polynomial.fit function
    nan_idx = np.isnan(x) | np.isnan(y)
    x = x[~nan_idx]
    y = y[~nan_idx]
    p = Polynomial.fit(x, y, degree).convert()
    return p


def richards_curve(x, a, k, b, nu, q, c):
    """Return Richard's curve for x with paras:
    x: variable
    a: lower (left) asymptote
    k: upper (right) asymptote when c=1
    b: growth rate
    nu: Shifts x-value of maximum growth
    q: related to value at x=0
    c: has influence on upper asymptote
    """
    return a + (k - a) / (c + q * np.exp(-b * x)**(1 / nu))


def dx_richards_curve(x, a, k, b, nu, q, c):
    """Return derivative for x Richard's curve with paras:
    x: variable
    a: lower (left) asymptote
    k: upper (right) asymptote when c=1
    b: growth rate
    nu: Shifts x-value of maximum growth
    q: related to value at x=0
    c: has influence on upper asymptote
    """
    numerator = b * q * (k - a) * np.exp(b * x / nu)
    denominator = nu * (c * np.exp(b * x / nu) + q)**2
    return numerator / denominator


def fit_richards(x, y, tolerance=0.1, kind=None):
    """
    Fit the strain data with a Richard's curve.

    Parameters
    ----------
    x
    y
    tolerance:
        Set the ratio of the tolerance window for the upper and lower
        bounds around the initital value
    kind:
        Type of data that is analyzed. Possible values are: None, 'stress',
        'strain'.

    Returns
    -------
        (a, k, b, nu, q, c)
    """
    a_init = np.mean(y[0])
    a_lower = a_init - tolerance * a_init
    if a_lower < 0:
        a_lower = 0
    a_upper = a_init + tolerance * a_init

    k_init = np.mean(y[-1])
    k_lower = k_init - tolerance * a_init
    k_upper = k_init + tolerance * a_init

    params = Parameters()
    if kind == 'stress':
        params.add('a', value=0)
        params['a'].vary = False
    else:
        params.add('a', value=a_init, min=a_lower, max=a_upper)
    params.add('k', value=k_init, min=k_lower, max=k_upper)
    params.add('b', value=3*k_init, min=k_init, max=10*k_init)
    params.add('nu', value=0.1, min=0, max=1)
    params.add('q', value=1, min=0, max=100)
    params.add('c', value=1, min=0, max=10)
    # params['c'].vary = False
    # create fit model
    fmodel = Model(richards_curve)

    fit_result = fmodel.fit(y, params, x=x, nan_policy='omit')

    a = fit_result.best_values['a']
    k = fit_result.best_values['k']
    b = fit_result.best_values['b']
    nu = fit_result.best_values['nu']
    q = fit_result.best_values['q']
    c = fit_result.best_values['c']
    return a, k, b, nu, q, c
