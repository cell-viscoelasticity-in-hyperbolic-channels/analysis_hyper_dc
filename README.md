# High-throughput viscoelastic characterization of cells in hyperbolic microchannels

This project provides information on how to recreate the figures in Reichel et al.
"High-throughput viscoelastic characterization of cells in hyperbolic 
microchannels", Lab on a Chip, 2024, 24, 2440-2453,
https://doi.org/10.1039/D3LC01061A

Some of the notebooks require the installation of the package `dctrack`. You 
can find more information on the 
[dctrack gitlab page](https://gitlab.gwdg.de/cell-viscoelasticity-in-hyperbolic-channels/dctrack). 

The data to recreate the figures are saved in the folder `data`. The analysis
to create the individual figure panels (`figure_plots`) can be found in the 
`notebooks` folder.

The raw data files for all experiments are saved on 
[figshare](https://figshare.com/collections/High-throughput_viscoelastic_characterization_of_cells_in_hyperbolic_microchannels/6983160): 
https://doi.org/10.6084/m9.figshare.c.6983160. The data analysis from raw data
to figure data is described in the `pre_processing` folder. The respective 
analysis notebooks are found in `pre_processing/tracking_and_summarize`. To run
the notebooks, clone this repository, download the raw data files from figshare
and unpack them into `pre_processing/raw_data`. Uploading all raw data to here
would make cloning and editing this repository impractical.

The jupyter notebooks were created using python version 3.11. An installation 
of python>=3.11 is recommended to run the notebooks.

The `requirements.txt` and `environment.yml` contain all dependencies for the 
notebooks to work and can be installed either with pip or conda. Please create 
a virtual environment, e.g. with Anaconda's `conda create` command and run the 
following commands:

Create a conda virtual environment from the environment.yml by running:

```
conda env create -f environment.yml
```

The default environment name will be `py311_hyperdc`.

Or from the requirements.txt:

```
# using Conda
conda create --name <env_name> --file requirements.txt
```

```
# using pip
pip install -r requirements.txt
```

